namespace Shapes.Tests;

public class Tests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void AreaOfCircle()
    {
        Circle circle = new Circle(0,0, 15);

        Assert.AreEqual(Math.PI*15*15, circle.Area());
    }

    [Test]
    public void AreaOfTriangle()
    {
        Triangle triangle = new Triangle(0, 0, 15, 0, 0, 15);

        Assert.AreEqual(15.0*15.0/2.0, triangle.Area());
    }

    [Test]
    public void AreaOfShape()
    {
        Shape shape;

        var date = DateTime.Now;

        shape = new Triangle(0, 0, 15, 0, 0, 15);
        Assert.AreEqual(15.0*15.0/2.0, shape.Area());

        shape = new Circle(0,0, 15);
        Assert.AreEqual(Math.PI*15.0*15.0, shape.Area());
    }

    [Test]
    public void RightTriangle()
    {
        Triangle triangle = new Triangle(0, 0, 15, 0, 0, 15);

        Assert.IsTrue(triangle.IsRight());
    }
}