namespace Shapes;


/// <summary>
/// Triangle shape
/// </summary>
public class Triangle : Shape
{
    /// <summary>
    /// Coordinates of first point of triangle 
    /// </summary>
    private double x1, y1;

    /// <summary>
    /// Coordinates of the second point of triangle 
    /// </summary>
    private double x2, y2;

    /// <summary>
    /// Coordinates of the third point of triangle 
    /// </summary>
    private double x3, y3;


    public Triangle(
     double x1, double y1,
     double x2, double y2,
     double x3, double y3)
    {
        this.x1=x1;
        this.y1=y1;
        this.x2=x2;
        this.y2=y2;
        this.x3=x3;
        this.y3=y3;
    }

    /// <summary>
    /// Gets area of a triangle S = 1/2 a*b*sin(alpha)
    /// </summary>
    /// <returns>Area of a triangle</returns>
    public override double Area()
    {
        var vector1 = new { x=x2-x1, y=y2-y1 };
        var vector2 = new { x=x3-x1, y=y3-y1 };

        double vector1len = Math.Sqrt(vector1.x * vector1.x + vector1.y * vector1.y);
        double vector2len = Math.Sqrt(vector2.x * vector2.x + vector2.y * vector2.y);

        double cos = (vector1.x * vector2.x + vector1.y * vector2.y) / 
        (vector1len * vector2len);

        double sin = Math.Sqrt(1 - cos * cos);

        double area = vector1len * vector2len * sin / 2;

        return area;

    }

    /// <summary>
    /// Checks if triangle is right
    /// </summary>
    /// <returns>True if right and False otherwise</returns>
    public bool IsRight()
    {
        var vector1 = new { x=x2-x1, y=y2-y1 };
        var vector2 = new { x=x3-x1, y=y3-y1 };

        double cos1 = (vector1.x*vector2.x + vector1.y * vector2.y);

        var vector3 = new { x=x1-x2, y=y1-y2 };
        var vector4 = new { x=x3-x2, y=y3-y2 };

        double cos2 = (vector3.x*vector4.x + vector3.y * vector4.y);

        var vector5 = new { x=x1-x3, y=y1-y3 };
        var vector6 = new { x=x2-x3, y=y2-y3 };

        double cos3 = (vector5.x*vector6.x + vector5.y * vector6.y);

        // TODO: Спорное сравнение с нулем надо придумать получше
        return (cos1*cos2*cos3) == 0;

    }
}