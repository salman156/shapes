namespace Shapes;

/// <summary>
/// Circle shape
/// </summary>
public class Circle : Shape
{
    /// <summary>
    /// Coordinates of circle centre
    /// </summary>
    double x, y;

    /// <summary>
    /// Radius of a circle
    /// </summary>
    double r;

    public Circle(double x, double y, double r)
    {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    /// <summary>
    /// Gets area of a circle S = pi * r^2
    /// </summary>
    /// <returns>Area of a circle</returns>
    public override double Area()
    {
        return Math.PI * r * r;
    }
}