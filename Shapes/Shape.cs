﻿namespace Shapes;

/// <summary>
/// Base class for Shapes
/// </summary>
public abstract class Shape
{
    /// <summary>
    /// Gets area of a shape
    /// </summary>
    /// <returns>Area of shape</returns>
    public abstract double Area();

}
